<?php
    require('Animal.php');
    require('Ape.php');
    require('Frog.php');

    $sheep = new Animal("shaun");

    echo "Name : $sheep->name <br>";  // "shaun"
    echo "Legs : $sheep->legs <br>";  // 4
    echo "cold blooded : $sheep->cold_blooded <br><br>";  // "no"

    $kodok = new Frog("buduk");
    echo "Name : $kodok->name <br>";  
    echo "Legs : $kodok->legs <br>";  
    echo "cold blooded : $kodok->cold_blooded <br>"; 
    $kodok->jump();  //  "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "<br><br> Name : $sungokong->name <br>";  
    echo "Legs : $sungokong->legs <br>";  
    echo "cold bloded : $sungokong->cold_blooded <br>"; 
    $sungokong->yell();  //  "Auooo"

?>